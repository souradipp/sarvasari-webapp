import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PreAuthRoutingModule } from './pre-auth-routing.module';
import { PreauthComponent } from './preauth.component';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [PreauthComponent, LoginComponent, ForgotPasswordComponent],
  imports: [
    CommonModule,
    PreAuthRoutingModule,
    SharedModule
  ]
})
export class PreAuthModule { }
