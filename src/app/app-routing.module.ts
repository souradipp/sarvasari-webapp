import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostauthComponent } from './post-auth/postauth.component';
import { PreauthComponent } from './pre-auth/preauth.component';

const routes: Routes = [
  { path: "", component: PreauthComponent, loadChildren: () => import('./pre-auth/pre-auth.module').then(m => m.PreAuthModule) },
  { path: "dashboard", component: PostauthComponent, loadChildren: () => import('./post-auth/post-auth.module').then(m => m.PostAuthModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
