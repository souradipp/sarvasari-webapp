import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
import { SidenavComponent } from './layouts/sidenav/sidenav.component';
import { TopNavComponent } from './layouts/top-nav/top-nav.component';



@NgModule({
  declarations: [SidenavComponent, TopNavComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  exports: [CommonModule, FormsModule, ReactiveFormsModule, MaterialModule, SidenavComponent, TopNavComponent]
})
export class SharedModule { }
